<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("1С-Битрикс: Управление сайтом");
?>

    <div class="first-screen">
        <div class="row columns first-screen-headlines">
            <h1>Расчётно-кассовое обслуживание в&nbsp;Москве</h1>
            <h2>При открытии счёта <span>до 25 мая</span> — три&nbsp;месяца бесплатного обслуживания</h2>
        </div>
        <div class="row columns first-screen-order">
            <div class="column medium-3 show-for-medium">&nbsp;</div>

            <div class="column small-12 medium-6 no-padding">
                <button class="orange-full order js-call-popup" js-call-popup-id="order">Открыть счёт</button>
            </div>

            <div class="column small-12 medium-3 large-2 end small-text-center">
                <p>Бронируем счёт за 30 минут</p>
            </div>
        </div>

        <div class="advantages-box blue-bk">
            <div class="row columns collapse">
                <div class="flex-advantages-container">
                    <div class="inner-flex">
                        <div class="icon spr-man"></div>
                        <p>Личный менеджер <br>
                            <span>консультация в любое время</span></p>
                    </div>
                    <div class="inner-flex small">
                        <div class="icon spr-star"></div>
                        <p>Бесплатно <br>
                            <span>открытие счёта</span></p>
                    </div>
                    <div class="inner-flex">
                        <div class="icon spr-time"></div>
                        <p>Операционный день <br>
                            <span>до 20:00</span></p>
                    </div>
                    <div class="inner-flex small">
                        <div class="icon spr-doc"></div>
                        <p>Интернет банк <br><span>быстрые переводы</span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="stages-screen" id="stages-screen">
        <div class="row column">
            <h3>Этапы открытия счёта</h3>
        </div>
        <div class="row column small-up-1 medium-up-2 large-up-4 stages-box">
            <div class="column">
                <h4>1. Отправьте заявку</h4>
                <p>Заполните заявку на сайте. Сотрудник перезвонит и&nbsp;забронирует счёт в течение 30&nbsp;минут.</p>
                <button class="link-orange js-call-popup" js-call-popup-id="order">Заполнить заявку</button>
            </div>
            <div class="column">
                <h4>2. Подготовьте документы</h4>
                <p>Сотрудники отправят список необходимых документов. Счётом можно начать пользоваться до подписания
                    договора.</p>
            </div>
            <div class="column">
                <h4>3. Подпишите договор</h4>
                <p>В течение 30 дней приезжайте в отделение банка, чтобы подписать и завизировать документы.</p>
            </div>
            <div class="column blue-border">
                <h4>Вы получите</h4>
                <p>Расчётно-кассовое обслуживание по тарифу «Стартовый +»</p>
            </div>
        </div>
    </div>

    <div class="tariff-starting-screen" id="tariff-starting-screen">
        <div class="row column">
            <h3>Тариф «Стартовый+» включает в&nbsp;себя</h3>
            <h4 class="descr">
                Бесплатно первые три месяца обслуживания при открытии счёта&nbsp;<span>до 25 мая</span>
            </h4>
            <h5>После третьего месяца, обслуживание счёта — 1000 ₽ в месяц</h5>
        </div>
        <div class="row tariff-starting-info-screen">
            <div class="column medium-5 large-4">
                <h4>Бесплатно</h4>
                <ul>
                    <li>открытие счёта</li>
                    <li>три месяца обслуживания счёта</li>
                    <li>изготовление и заверение копий документовна открытие счета</li>
                    <li>удостоверение подписей в карточке с образцами подписей и оттиска печати</li>
                </ul>
            </div>
            <div class="column medium-7 large-8">
                <h4>Выгодно</h4>
                <ul>
                    <li>подключение и обслуживание системы Интернет-Банк</li>
                    <li>десять внешних неналоговых платежей ежемесячно</li>
                    <li>налоговые платежи и др. в бюджеты — без ограничений</li>
                    <li>чековая книжка</li>
                    <li>снятие наличных - 100 000 ₽ ежемесячно</li>
                </ul>
            </div>
            <div class="column">
                <button class="btn-orange js-call-popup" js-call-popup-id="order">Открыть счёт</button>
            </div>
        </div>
    </div>

    <div class="form-big-screen">
        <div class="row column">
            <h3>Бронируем счёт за 30 минут в Москве</h3>
        </div>
        <div class="advantages-box">
            <div class="row columns collapse">
                <div class="flex-advantages-container">
                    <div class="inner-flex">
                        <div class="icon spr-man"></div>
                        <p>Личный менеджер <br>
                            <span>консультация в любое время</span></p>
                    </div>
                    <div class="inner-flex small">
                        <div class="icon spr-star"></div>
                        <p>Бесплатно <br>
                            <span>открытие счёта</span></p>
                    </div>
                    <div class="inner-flex">
                        <div class="icon spr-time"></div>
                        <p>Операционный день <br>
                            <span>до 20:00</span></p>
                    </div>
                    <div class="inner-flex small">
                        <div class="icon spr-doc"></div>
                        <p>Интернет банк <br><span>быстрые переводы</span></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row columns">
            <div class="form-big-box">
                <h4>Откройте счёт <span>до 25 мая</span> и получите три месяца бесплатного обслуживания</h4>
                <?
                $APPLICATION->IncludeComponent(
                    "it-agency:iblock.form.ajax",
                    "big-form",
                    array(
                        "COMPONENT_TEMPLATE" => "big-form",
                        "INPUT_ID" => "",
                        "INPUT_CLASS" => "",
                        "INPUT_NAME" => "",
                        "INPUT_VALUE" => "",
                        "LABEL_TEXT" => "",
                        "UNIQUE_ID" => "order_opening_account_in_doc",
                        "IBLOCK_TYPE" => "order",
                        "IBLOCK_ID" => "2",
                        "PROPERTY_CODES" => array(
                            0 => "2",
                            1 => "3",
                            2 => "4",
                            3 => "5",
                            4 => "6",
                        ),
                        "PROPERTY_CODES_REQUIRED" => array(
                            0 => "2",
                            1 => "3",
                            2 => "4",
                            3 => "5",
                            4 => "6",
                        ),
                        "CALLBACK_JS" => "console.log('Успешно отправлена форма order_opening_account_in_doc')",
                        "CALLBACK_PHP" => "\$letters = array('я', 'о'); \$fruit   = array('яблоко', 'орех'); \$text    = 'я о'; \$output  = str_replace(\$letters, \$fruit, \$text); echo \$output;"
                    ),
                    false
                );
                ?>

            </div>
        </div>
    </div>


    <div id="js-popup-wrapper"></div>

    <div class="js-popup column medium-6 large-4" id="popup-thank">
        <button class="js-popup-close popup-close"></button>
        <div class="js-popup-content">
            <h4>Спасибо!</h4>
            <p>Специалист перезвонит вам по номеру</p>
            <p class="phone"></p>
            <p>в течение 10 минут</p>
            <button class="js-popup-close btn-orange">Хорошо</button>
        </div>
    </div>

    <div class="js-popup column medium-6 large-4" id="popup-tell-my">
        <button class="js-popup-close popup-close"></button>
        <div class="js-popup-content">
            <h4 class="left">Заявка на обратный звонок</h4>
            <?
            $APPLICATION->IncludeComponent(
                "it-agency:iblock.form.ajax",
                "tell-my",
                array(
                    "COMPONENT_TEMPLATE" => "tell-my",
                    "INPUT_ID" => "",
                    "INPUT_CLASS" => "",
                    "INPUT_NAME" => "",
                    "INPUT_VALUE" => "",
                    "LABEL_TEXT" => "",
                    "UNIQUE_ID" => "order_tell_my",
                    "IBLOCK_TYPE" => "order",
                    "IBLOCK_ID" => "1",
                    "PROPERTY_CODES" => array(
                        0 => "1",
                        1 => "7",
                    ),
                    "PROPERTY_CODES_REQUIRED" => array(
                        0 => "1",
                        1 => "7",
                    ),
                    "CALLBACK_JS" => "console.log('Успешно отправлена форма order_tell_my')",
                    "CALLBACK_PHP" => "\$letters = array('я', 'о'); \$fruit   = array('яблоко', 'орех'); \$text    = 'я о'; \$output  = str_replace(\$letters, \$fruit, \$text); echo \$output;"
                ),
                false
            );
            ?>
        </div>
    </div>


    <div class="js-popup column medium-6 large-4" id="popup-order">
        <button class="js-popup-close popup-close"></button>
        <div class="js-popup-content">
            <h4>Откройте счёт <span>до 25 мая</span> и получите три месяца бесплатного обслуживания</h4>
            <?
            $APPLICATION->IncludeComponent(
                "it-agency:iblock.form.ajax",
                "big-form-popup",
                array(
                    "COMPONENT_TEMPLATE" => "big-form",
                    "INPUT_ID" => "",
                    "INPUT_CLASS" => "",
                    "INPUT_NAME" => "",
                    "INPUT_VALUE" => "",
                    "LABEL_TEXT" => "",
                    "UNIQUE_ID" => "order_opening_account_in_popup",
                    "IBLOCK_TYPE" => "order",
                    "IBLOCK_ID" => "2",
                    "PROPERTY_CODES" => array(
                        0 => "2",
                        1 => "3",
                        2 => "4",
                        3 => "5",
                        4 => "6",
                    ),
                    "PROPERTY_CODES_REQUIRED" => array(
                        0 => "2",
                        1 => "3",
                        2 => "4",
                        3 => "5",
                        4 => "6",
                    ),
                    "CALLBACK_JS" => "console.log('Успешно отправлена форма order_opening_account_in_popup')",
                    "CALLBACK_PHP" => "\$letters = array('я', 'о'); \$fruit   = array('яблоко', 'орех'); \$text    = 'я о'; \$output  = str_replace(\$letters, \$fruit, \$text); echo \$output;"
                ),
                false
            );
            ?>
        </div>
    </div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>