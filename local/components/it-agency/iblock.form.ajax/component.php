<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if (!CModule::IncludeModule("iblock"))
    return;

if (!function_exists('it_agency_iblock_form_ajax_cmp')) {
    //TODO: переписать под короткие условия
    function it_agency_iblock_form_ajax_cmp($a, $b)
    {
        if ($a['SORT'] == $b['SORT'])
            return 0;
        elseif ($a['SORT'] > $b['SORT'])
            return 1;
        else
            return -1;
    }
}

$arResult['SEARCH_RES'] = array();

if ($_REQUEST['AJAX'] == $arParams['UNIQUE_ID']) {
    $GLOBALS['APPLICATION']->RestartBuffer();
    $PROP = array();
    $sendData = '';


    foreach ($_POST as $key => $value) {
        $res = CIBlockProperty::GetByID($key, $arParams['IBLOCK_ID'], $arParams['PROPERTY_CODES']);

        //А может ли юзвери добавить это поле?
        if ($ar_res = $res->GetNext()) {
            if (array_keys($arParams['PROPERTY_CODES'], $ar_res['ID']))
            {
                $PROP[$ar_res['ID']] = $value;
                $sendData .= $ar_res['NAME'].': '.$value."\n\r";
            }

        }
    }

    $el = new CIBlockElement;

    $arLoadProductArray = Array(
        "ACTIVE_FROM" => date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), time()),
        "MODIFIED_BY" => 1,
        "IBLOCK_SECTION_ID" => false,
        "IBLOCK_ID" => $arParams['IBLOCK_ID'],
        "PROPERTY_VALUES" => $PROP,
        "NAME" => "Заявка от " . date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), time()),
        "ACTIVE" => "Y",
    );

    $iblock_info = CIBlock::GetByID($arParams['IBLOCK_ID']);
    $arIblockInfo = $iblock_info->GetNext();


    if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
        //TODO: Подумать - как проверить

        $arEventFields = array(
            "MAIL_THEME" => $arIblockInfo['NAME'],
            "CONTENT" => $sendData,
        );
        CEvent::SendImmediate("FORM_DATA_SEND", "s1", $arEventFields);

        eval($arParams['CALLBACK_PHP']);
    }
    //TODO: Перехват ошибок



    $this->IncludeComponentTemplate();
    die();
} else {
    $property_list = array();

    foreach ($arParams['PROPERTY_CODES'] as $propID) {
        $res = CIBlockProperty::GetByID($propID);
        if ($ar_res = $res->GetNext())
            $property_list[$propID] = array(
                'ID' => $ar_res["ID"],
                'NAME' => $ar_res["NAME"],
                'CODE' => $ar_res["CODE"],
                'SORT' => $ar_res["SORT"],
                'RECURRED' => 'N'
            );
    }

    foreach ($arParams['PROPERTY_CODES_REQUIRED'] as $propID) {
        if(isset($property_list[$propID]))
            $property_list[$propID]['RECURRED'] = 'Y';
    }

    //Сортируем поля по свойству sort
    usort($property_list, "it_agency_iblock_form_ajax_cmp");

    $arResult['FIELDS'] = $property_list;
    $arResult['PARAMS'] = $arParams;
    $arResult['PARAMS']['CALLBACK_JS'] = str_replace('\'','"',$arResult['PARAMS']['CALLBACK_JS']);

    $arResult['UFID'] = uniqid($arParams['UNIQUE_ID']);
    $arResult['unicWrapperID'] = uniqid("userSearchAjax_");
    $arResult['unicControllerID'] = uniqid("userControllerID_");
    $this->IncludeComponentTemplate();
}
?>