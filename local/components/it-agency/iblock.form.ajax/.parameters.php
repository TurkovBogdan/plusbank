<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if(!CModule::IncludeModule("iblock"))
    return;

if($arCurrentValues["IBLOCK_ID"] > 0)
{
    $arIBlock = CIBlock::GetArrayByID($arCurrentValues["IBLOCK_ID"]);

    $bWorkflowIncluded = ($arIBlock["WORKFLOW"] == "Y") && CModule::IncludeModule("workflow");
    $bBizproc = ($arIBlock["BIZPROC"] == "Y") && CModule::IncludeModule("bizproc");
}
else
{
    $bWorkflowIncluded = CModule::IncludeModule("workflow");
    $bBizproc = false;
}

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arIBlock=array();
$rsIBlock = CIBlock::GetList(Array("sort" => "asc"), Array("TYPE" => $arCurrentValues["IBLOCK_TYPE"], "ACTIVE"=>"Y"));
while($arr=$rsIBlock->Fetch())
{
    $arIBlock[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];
}

$arProperty_LNSF = array(

);

$arVirtualProperties = $arProperty_LNSF;

$rsProp = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"desc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arCurrentValues["IBLOCK_ID"]));

while ($arr=$rsProp->Fetch())
{
    $arProperty[$arr["ID"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
    if (in_array($arr["PROPERTY_TYPE"], array("L", "N", "S", "F")))
    {
        $arProperty_LNSF[$arr["ID"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
    }
}

$arGroups = array();
$rsGroups = CGroup::GetList($by="c_sort", $order="asc", Array("ACTIVE" => "Y"));
while ($arGroup = $rsGroups->Fetch())
{
    $arGroups[$arGroup["ID"]] = $arGroup["NAME"];
}

if ($bWorkflowIncluded)
{
    $rsWFStatus = CWorkflowStatus::GetList($by="c_sort", $order="asc", Array("ACTIVE" => "Y"), $is_filtered);
    $arWFStatus = array();
    while ($arWFS = $rsWFStatus->Fetch())
    {
        $arWFStatus[$arWFS["ID"]] = $arWFS["TITLE"];
    }
}
else
{
    $arActive = array("ANY" => GetMessage("IBLOCK_STATUS_ANY"), "INACTIVE" => GetMessage("IBLOCK_STATUS_INCATIVE"));
    $arActiveNew = array("N" => GetMessage("IBLOCK_ALLOW_N"), "NEW" => GetMessage("IBLOCK_ACTIVE_NEW_NEW"), "ANY" => GetMessage("IBLOCK_ACTIVE_NEW_ANY"));
}

$arAllowEdit = array("CREATED_BY" => GetMessage("IBLOCK_CREATED_BY"), "PROPERTY_ID" => GetMessage("IBLOCK_PROPERTY_ID"));


$arComponentParameters = array(
"GROUPS" => array(),
"PARAMETERS" => array(
    "IBLOCK_TYPE" => array(
        "PARENT" => "DATA_SOURCE",
        "NAME" => GetMessage("IBLOCK_TYPE"),
        "TYPE" => "LIST",
        "ADDITIONAL_VALUES" => "Y",
        "VALUES" => $arIBlockType,
        "REFRESH" => "Y",
    ),
    "UNIQUE_ID" => array(
        "PARENT" => "BASE",
        "NAME" => GetMessage("CP_BCSF_UNIQUE_ID"),
        "TYPE" => "STRING",
        "MULTIPLE" => "N",
        "DEFAULT" => "",
        "REFRESH" => "N",
    ),
    "IBLOCK_ID" => array(
        "PARENT" => "DATA_SOURCE",
        "NAME" => GetMessage("IBLOCK_IBLOCK"),
        "TYPE" => "LIST",
        "ADDITIONAL_VALUES" => "Y",
        "VALUES" => $arIBlock,
        "REFRESH" => "Y",
    ),
    "PROPERTY_CODES" => array(
        "PARENT" => "DATA_SOURCE",
        "NAME" => GetMessage("IBLOCK_PROPERTY"),
        "TYPE" => "LIST",
        "MULTIPLE" => "Y",
        "VALUES" => $arProperty_LNSF,
    ),
    "PROPERTY_CODES_REQUIRED" => array(
        "PARENT" => "DATA_SOURCE",
        "NAME" => GetMessage("IBLOCK_PROPERTY_REQUIRED"),
        "TYPE" => "LIST",
        "MULTIPLE" => "Y",
        "ADDITIONAL_VALUES" => "N",
        "VALUES" => $arProperty_LNSF,
    ),
    "CALLBACK_JS" => array(
        "PARENT" => "BASE",
        "NAME" => GetMessage("CP_BCSF_CALLBACK_JS"),
        "TYPE" => "STRING",
        "MULTIPLE" => "N",
        "DEFAULT" => "",
        "REFRESH" => "N",
    ),
    "CALLBACK_PHP" => array(
        "PARENT" => "BASE",
        "NAME" => GetMessage("CP_BCSF_CALLBACK_PHP"),
        "TYPE" => "STRING",
        "MULTIPLE" => "N",
        "DEFAULT" => "",
        "REFRESH" => "N",
    ),
),
);
?>