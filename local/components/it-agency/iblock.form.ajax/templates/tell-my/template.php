<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
if ($_REQUEST['AJAX'] == $arParams['UNIQUE_ID']) {
    
} else {
    ?>
    <form class="form-tell-my-content js-design-labels" id="<?= $arResult['PARAMS']['UNIQUE_ID'] ?>">
        <? foreach ($arResult['FIELDS'] as $field): ?>
            <?
            $type = 'text';
            if(strtolower($field['CODE'])=='phone')
                $type = 'tel';
            if(strtolower($field['CODE']) == 'mail')
                $type = 'email';
            ?>
            <fieldset class="active">
                <label for="<?= $field['CODE'] . '-' . $arResult['UFID'] ?>"><?= $field['NAME'] ?></label>
                <input id="<?= $field['CODE'] . '-' . $arResult['UFID'] ?>" name="<?= $field['CODE'] ?>" type="<?=$type?>"
                       autocomplete="off"<?= ($field['RECURRED'] == 'Y') ? ' required' : '' ?>/>
            </fieldset>

        <? endforeach; ?>
        <fieldset class="submit-zone">
            <button type="submit" name="AJAX" value="<?= $arParams['UNIQUE_ID'] ?>" class="btn-orange">Открыть счёт</button>
        </fieldset>
        <p>Перезвоним в течение 10&nbsp;минут</p>
    </form>

    <script>
        $(document).ready(function () {
            if (typeof formController === 'undefined' || formController === null) {
                formController = [];
            }
            formController.push(new ita_form_send_controller('', '<?=$arResult['PARAMS']['UNIQUE_ID']?>', '<?=$arResult['PARAMS']['CALLBACK_JS']?>'));

            formController[formController.length - 1].showFormResult = function () {
                var data = this.sendedData[1].value;
                $('#popup-thank .phone').html(data);
                obPopup.replacePopup('#popup-thank');
            }
        });
    </script>
    <?
}
?>