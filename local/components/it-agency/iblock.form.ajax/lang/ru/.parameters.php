<?
$MESS["CP_BCSF_UNIQUE_ID"] = "Уникальный ID";
$MESS["CP_BCSF_UNIQUE_ID_TIP"] = "Должен быть уникальным для каждого компонента на странице";

$MESS["CP_BCSF_CALLBACK_JS"] = "callback js";
$MESS["CP_BCSF_CALLBACK_PHP"] = "callback php";

$MESS["IBLOCK_TYPE"] = "Тип инфоблока";
$MESS["IBLOCK_IBLOCK"] = "Инфоблок";
$MESS["IBLOCK_PROPERTY"] = "Выводимые поля";
$MESS["IBLOCK_PROPERTY_REQUIRED"] = "Обязательные для заполнения поля";
?>