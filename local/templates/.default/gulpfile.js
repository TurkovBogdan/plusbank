var gulp = require('gulp'),
    path = require('path'),
//    less = require('gulp-less'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cssmin = require('gulp-cssmin'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    include = require('gulp-include'),
//    replace = require('gulp-replace-path'),
    plumber = require('gulp-plumber'),
    csscomb = require('gulp-csscomb'),
//    imagemin = require('gulp-imagemin'),
    watch = require('gulp-watch'),
    batch = require('gulp-batch'),
    uglify = require('gulp-uglify')
//    spritesmith = require('gulp.spritesmith')
//    livereload = require('gulp-livereload'),
//    colors = require('colors')
    ;
//require('es6-promise');//gulp-autoprefixer - fix

var parker = require('gulp-parker2');

conf = {
    js: {
        dir: './js/',
        src: ['./js/main.js'],
        output: './',
        name: 'scripts.js',
        minName: {
            //basename: "",                         //Изменить имя при минификации
            //prefix: "min.",                           //Добавить префикс
            suffix: '.min',                         //Добавить постфикс
            //extname: ""                           //Изменить расширение файла
        }
    },
    css: {
        dir: './',
        src: [
            './scss/main.scss'
        ],
        output: './',                           //Куда сохранять собранные стили
        name: 'template_styles.css',                          //Имя файла на выходе
        minName: {
            //basename: "",                         //Изменить имя при минификации
            prefix: "min.",                           //Добавить префикс
            //suffix: '.min',                         //Добавить постфикс
            //extname: ""                           //Изменить расширение файла
        }
    },
    sass: {
        include: [
            '.bower_components/foundation-sites/scss',
            '.bower_components/motion-ui/src',
            'scss'
        ],
        broserFixDir: [
            './scss/browsers/*.scss'
        ]
    },
    minSurf: '.min',
    apLast: 'last 20 versions'
};

gulp.task('script-compile', function () {
    return gulp
        .src(conf.js.src)
        .pipe(plumber())//Перехват ошибок
        .pipe(include())//Инклуды
        .pipe(uglify())//Минификация
        .pipe(concat(conf.js.name))//Обьединение
        .pipe(gulp.dest(conf.js.output))
        .pipe(rename(conf.js.minName))
        .pipe(gulp.dest(conf.js.output));
});

gulp.task('style-compile', function () {
    gulp
        .src(conf.css.src)
        .pipe(plumber())                        //Перехват ошибок
        .pipe(concat(conf.css.name))            //Обьединение файлов в 1
        .pipe(sass({                            //
            includePaths: conf.sass.include         //Перечень путей, в которой ищется файл при использовании import
        }))
        .pipe(autoprefixer({                    //Автопрефиксы для браузеров
            browsers: [conf.apLast],
            cascade: false
        }))
        .pipe(csscomb())                        //Сортируем стили
        .pipe(gulp.dest(conf.css.output))       //Сохраняем полную версию стилей
        .pipe(cssmin())                         //Минифицируем код
        .pipe(rename(conf.css.minName))         //Изменить имя минифицированной версии
        .pipe(gulp.dest(conf.css.output))       //Сохраняем минифицированную версию
    ;

    gulp.src(conf.sass.broserFixDir)
        .pipe(sass({
            includePaths: conf.sass.include
        }))
        .pipe(autoprefixer({                    //Автопрефиксы для браузеров
            browsers: [conf.apLast],
            cascade: false
        }))
        .pipe(csscomb())
        .pipe(cssmin())
        .pipe(gulp.dest('./'));
});

gulp.task('style-compile-browsers', function () {

});

gulp.task('default', ['script-compile', 'style-compile'], function () {
    watch([
            './js/*.js',
            './js/*/*.js',
            '!./js/script.js',
            '!./js/script.min.js'
        ],
        batch(function (events, cb) {
            gulp.start('script-compile', cb);
        }));

    watch([
            './scss/.vendor/*.css',
            './css/.vendor/*.scss',
            './scss/*.css',
            './scss/*/*.scss',
            './scss/*.scss'
        ],
        batch(function (events, cb) {
            gulp.start('style-compile', cb);
        }));
});

gulp.task('parker', function() {
    return gulp.src('./template_styles.css')
        .pipe(parker());
});