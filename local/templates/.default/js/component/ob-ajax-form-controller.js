/**
 * Функция-конструктор управляющая AJAX формами
 *
 * Автор: Турков богдан
 * Почта: turkov.bogdan@gmail.com
 *
 * Требования:
 * jQuery
 * jquery.transit (при использовании штатной анимации)
 *
 * TODO: перенести настройки в массив
 * TODO: добавить настройку для запрета отправки формы после успешной отправки
 *
 * @param sendURL
 * @param formID
 * @param onSuccessfulSending
 */
function ita_form_send_controller(sendURL, formID, onSuccessfulSending) {
    this.sendURL = sendURL;
    this.formID = formID;
    this.onSuccessfulSending = onSuccessfulSending;

    this.init();
}

ita_form_send_controller.prototype = {
    sendURL: null,
    formID: null,
    sendProgress: false,
    sendedData: null,
    yaCounter: false,
    onSuccessfulSending: null,
    sended: false,

    init: function () {
        this.bindEvents();
        return true;
    },

    /**
     * Повесить события
     */
    bindEvents: function () {
        var $ob = this;
        var $form = this.formID;
        $('#' + this.formID).find('button[type="submit"]').on("click", {ob: $ob, form: $form}, $ob.onSubmit);
    },

    /**
     * Обработка отправки формы
     * @param event
     * @returns {boolean}
     */
    onSubmit: function (event) {
        var $ob = event.data.ob;
        var $form = event.data.form;

        if ($ob.sendProgress == false) {
            var checkRes = $ob.checkForm('#' + $form);
            $ob.resetPointWrongField('#' + $form);

            if (checkRes.error == true) {
                $ob.pointWrongField(checkRes.errorField);
                $ob.sendProgress = false;
            }
            else {
                $ob.sendProgress = true;
                $ob.showPreloader($form);
                $ob.sendData($form, checkRes.data);
            }
        }
        return false;
    },

    /**
     * Отобразить прелоадер формы
     * @param $form
     */
    showPreloader: function ($form) {

    },

    /**
     * Скрыть прелоадер формы
     * @param $form
     */
    hidePreloader: function ($form) {

    },

    /**
     * Отобразить результат отправки формы
     * @param $form
     */
    showFormResult: function ($form) {

    },

    /**
     * Выделение незаполненных или ошибочных полей
     * @param filed
     */
    pointWrongField: function (filed) {
        $(filed).each(function () {
            $(this).addClass('error');
            $(this).parent('fieldset').find('label')
                .transition({x: '5px'}, 100, 'snap')
                .transition({x: '-5px'}, 100, 'snap')
                .transition({x: '5px'}, 100, 'snap')
                .transition({x: '-5px'}, 100, 'snap')
                .transition({x: '0px'}, 100, 'snap');
        });
    },

    /**
     * Сброс выделения незаполненых или ошибочных полей
     * @param filed
     */
    resetPointWrongField: function (form) {
        $(form).find('input').each(function () {
            $(this).removeClass('error');
        });
    },

    /**
     * Функция проверки и валидации полей
     * @param $form
     * @returns {{error: boolean, data: Array, errorField: Array}}
     */
    checkForm: function ($form) {
        var formData = {
            error: false,
            data: [],
            errorField: []
        };

        $($form).find("input").each(function () {
            var req = $(this).attr('required');
            if (typeof req !== typeof undefined && req !== false) {
                if ($(this).val() == '') {
                    formData.error = true;
                    formData.errorField.push(this);
                }
            }
            if( $(this).attr('type') == 'email' )
            {
                var  value = $(this).val();
                if (value.indexOf('@') == -1) {
                    formData.error = true;
                    formData.errorField.push(this);
                }
            }
        });

        this.sendedData = $($form).serializeArray();
        console.log(this.sendedData);

        formData.data = $($form).serialize();
        formData.data += '&AJAX=' + this.formID;

        return formData;
    },

    /**
     * Отправка данных
     * @param $form
     * @param data
     */
    sendData: function ($form, data) {
        $ob = this;
        $.ajax({
            method: "POST",
            url: this.sendURL,
            data: data,
            cache: false
        })
            .done(function (data) {
                console.log(data);
                if (data == 'false')
                    $ob.onSendError();
                else {
                    $ob.hidePreloader($form);
                    $ob.showFormResult($form);
                    
                    if($ob.onSuccessfulSending != null)
                        eval($ob.onSuccessfulSending);//TODO: Подумать - как проверить

                    $ob.sendProgress = false;
                    $ob.sended = true;
                }
            })
            .fail(function () {
                $ob.onSendError();
            });
    },

    /**
     * При возникновении ошибки отправки на стороне сервера
     */
    onSendError: function () {

    },
}