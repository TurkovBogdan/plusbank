$(document).ready(function () {
    $( ".js-design-labels input" ).on( "focus", function() {
        $(this).parent('fieldset').addClass('focused');
    });
    $( ".js-design-labels input" ).on( "focusout", function() {
        if( $(this).val() == '' )
            $(this).parent('fieldset').removeClass('focused');
    });
});