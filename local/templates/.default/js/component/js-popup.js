obPopup = {
    init: function () {
        this.bindEvents();
    },
    showPopup: function () {
        var popupHeight = $(this.elPopup).outerHeight();
        var marginTop = popupHeight / -2;
        $(this.elPopup).css('margin-top', marginTop)
            .addClass('open')
            .fadeIn(700);
        $('#js-popup-wrapper').fadeIn(700);
    },
    showPopupByID: function (ID) {
        var popupHeight = $(ID).outerHeight();
        var marginTop = popupHeight / -2;
        $(ID).css('margin-top', marginTop)
            .addClass('open')
            .fadeIn(700);
        $('#js-popup-wrapper').fadeIn(700);
    },
    replacePopup: function (ID) {
        $('.js-popup').removeClass('open').fadeOut(400);
        var popupHeight = $(ID).outerHeight();
        var marginTop = popupHeight / -2;
        $(ID).css('margin-top', marginTop)
            .addClass('open')
            .fadeIn(700);
    },
    updatePosition: function () {//:TODO Добавлять класс hasActivePopup к body (проверка без перебора)
        $(".js-popup").each(function () {
            if ($(this).hasClass('open')) {
                var popupHeight = $(this).outerHeight();
                var marginTop = popupHeight / -2;
                $(this).css('margin-top', marginTop);
            }
        });
    },
    closePopup: function () {
        $('#js-popup-wrapper').fadeOut(400);
        $('#js-popup-arrow').fadeOut(400);
        $('.js-popup').removeClass('open').fadeOut(400);
    },

    bindEvents: function () {
        var $ob = this;
        $(".js-popup-close").on("click", function () {
            $ob.closePopup();
            return false;
        });

        $("#js-popup-wrapper").on("click", function () {
            $ob.closePopup();
            return false;
        });

        $(window, document).resize(function () {
            $ob.updatePosition();
        });

        $(".js-call-popup").on("click", function () {
            var popupID = $(this).attr('js-call-popup-id');
            if (popupID != null && popupID != '') {
                $ob.showPopupByID('#popup-' + popupID);
            }
            return false;
        });
    }
};


$(document).ready(function () {
    obPopup.init();
});
