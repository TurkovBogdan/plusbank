//=require ../.bower_components/jquery/dist/jquery.js
//=require ../.bower_components/what-input/what-input.js
//=require ../.bower_components/jquery.transit/jquery.transit.js
//=require ../.bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js


//=require component/js-design-labels.js
//=require component/ob-ajax-form-controller.js
//=require component/js-popup.js

$(document).ready(function () {
    $('input[type=tel]').mask("+7 999 999-99-99");


    $('nav a').click(function () {
        var scroll_el = $(this).attr('href');
        if ($(scroll_el).length != 0) {
            $('html, body').animate({scrollTop: $(scroll_el).offset().top-60}, 500);
        }
        return false;
    });

});

