<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader;
?>
<!DOCTYPE HTML>
<html class="no-js" lang="ru">
<head>
    <title><? $APPLICATION->ShowTitle() ?></title>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?
    //$APPLICATION->ShowMeta("robots", false, true);
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/scripts.js');
    $APPLICATION->ShowHead();
    ?>
    <!--[if IE 9]>
    <link href="<?=SITE_TEMPLATE_PATH?>/ie9.css" rel="stylesheet">
    <![endif]-->
</head>
<body<? if ($USER->IsAdmin()):global $USER;
    echo ' class="admin"';endif; ?>>
<? $APPLICATION->ShowPanel(); ?>

<div class="menu-fixed">
    <div class="row columns collapse">
        <div class="column small-4 medium-4 large-8">
            <img class="logo" src="<?= SITE_TEMPLATE_PATH ?>/images/logo.jpg">
            <img class="logo-mobile" src="<?= SITE_TEMPLATE_PATH ?>/images/mobile-logo.jpg">
            <nav>
                <? $APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"clear", 
	array(
		"ROOT_MENU_TYPE" => "top",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "top",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"COMPONENT_TEMPLATE" => "clear"
	),
	false
); ?>
            </nav>
        </div>
        <div class="column small-8 medium-8 large-4">
            <div class="menu-fixed-phone">
                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => "include/phone.php",
                )); ?>
                <button class="link-blue js-call-popup" js-call-popup-id="tell-my">Заказать обратный звонок</button>
            </div>
            <div class="menu-fixed-working-hours">
                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => "include/working-hours.php",
                )); ?>
                <button class="link-blue">Адреса офисов</button>
            </div>
        </div>
    </div>
</div>